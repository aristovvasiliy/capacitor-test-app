import React, {Component} from "react";
import GeoLogService from "../services/GeoLogStorage";

class GeolocationLog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            log: ''
        };

        this.getLog = this.getLog.bind(this);
    }

    componentDidMount() {
        setInterval(this.getLog, 1000);
    }

    getLog = async () => {
        const log = await GeoLogService.get();
        this.setState({log: log})
    }

    render() {
        return (
            <div style={{whiteSpace: 'pre-line', border: '1px solid #000'}}>
                <br/>
                {this.state.log}
            </div>
        );
    }
}

export default GeolocationLog;
