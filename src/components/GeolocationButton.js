import {Plugins} from '@capacitor/core';
import {IonButton} from "@ionic/react";
import React, {Component} from "react";
import GeoLogService from "../services/GeoLogStorage";
const {Geolocation} = Plugins;

class GeolocationButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            position: ''
        };
    }

    getCurrentPosition = () => {
        Geolocation.getCurrentPosition().then((coordinates) => {
            this.setState({position: coordinates.coords.latitude + ', ' + coordinates.coords.longitude})
            GeoLogService.saveCoords(coordinates.coords.latitude, coordinates.coords.longitude);
        });
    }

    clearGeoLog = () => {
        GeoLogService.clearFile();
    }

    // watchPosition = () => {
    //     const wait = Geolocation.watchPosition({}, (position, err) => {
    //     })
    // }

    render() {
        return (
            <div>
                <hr/>
                <IonButton color="primary" onClick={this.getCurrentPosition}>Get Location</IonButton>
                <IonButton color="primary" onClick={this.clearGeoLog}>Clear geo log</IonButton>
                <hr/>
            </div>
        );
    }
}

export default GeolocationButton;
