import {CameraResultType, Plugins} from '@capacitor/core';
import {IonButton, IonImg} from "@ionic/react";
import React, {Component} from "react";
import {defineCustomElements} from "@ionic/pwa-elements/loader";

const {Camera} = Plugins;

class Shot extends Component {
    constructor(props) {
        super(props);
        this.state = {photo: ''};
        defineCustomElements(window);
    }

    async takePicture() {
        const image = await Camera.getPhoto({
            quality: 90,
            allowEditing: false,
            resultType: CameraResultType.Uri
        });
        var imageUrl = image.webPath;
        // Can be set to the src of an image now
        this.setState({
            photo: imageUrl
        })
    }

    render() {
        const {photo} = this.state;
        return (
            <div>
                {
                    photo &&
                    <IonImg style={{
                        'border': '1px solid black',
                        'height': '300px'
                    }}
                            src={photo}></IonImg>
                }
                <br/>
                <IonButton color="primary" onClick={() => this.takePicture()}>Take a shot</IonButton>
            </div>
        );
    }
}

export default Shot;
