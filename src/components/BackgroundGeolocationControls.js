import React, {Component} from "react";
import {IonButton} from "@ionic/react";
import BgGeoLocation from "../services/BgGeoLocation";
import {Toast} from "@capacitor/core";

class BackgroundGeolocationControls extends Component {
    constructor(props) {
        super(props);

        this.bgGeoLocation = new BgGeoLocation();
    }

    startBackgroundGeolocation = async () => {
        await this.bgGeoLocation.start();
    }

    stopBackgroundGeolocation = async () => {
        await this.bgGeoLocation.stop();
    }

    showUnsetLocations = async () => {
        let locations = await this.bgGeoLocation.getUnsentLocations();
        await Toast.show({text: locations, duration: 'short'});
    }

    deleteUnsetLocations = async () => {
        await this.bgGeoLocation.deleteUnsentLocations();
    }

    render() {
        return (
            <div>
                <IonButton color="primary" onClick={this.startBackgroundGeolocation}>
                    Start background geolocation
                </IonButton>
                <br/>
                <IonButton color="danger" onClick={this.stopBackgroundGeolocation}>
                    Stop background geolocation
                </IonButton>
                <br/>
                <IonButton color="primary" onClick={this.showUnsetLocations}>
                    Show unsent locations
                </IonButton>
                <br/>
                <IonButton color="primary" onClick={this.deleteUnsetLocations}>
                    Delete unsent locations
                </IonButton>
            </div>
        );
    }
}

export default BackgroundGeolocationControls;
