import {Plugins} from '@capacitor/core';

const {Storage, Toast} = Plugins;

class GeoLogService {
    saveCoords = async (lat, lon) => {
        let key = new Date().toString() + ' ' + lat + ', ' + lon + '\n\r';

        await Toast.show({text: 'Coords: ' + lat + ', ' + lon, duration: 'short'});

        await Storage.set({
            key: key,
            value: new Date().toString()
        });
    }

    clearFile = async () => {
        await Storage.clear();
        await Toast.show({text: 'Storage clear', duration: 'short'});
    }

    get = async () => {
        let file = '';

        const {keys} = await Storage.keys();
        keys.forEach((key) => {
            file += key;
        })

        return file;
    }

}

export default new GeoLogService();
