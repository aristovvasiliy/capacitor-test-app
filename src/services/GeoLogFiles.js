import {FilesystemDirectory, FilesystemEncoding, Plugins} from '@capacitor/core';

const {Filesystem, Toast} = Plugins;

const filePath = 'geolog.txt';
const directory = FilesystemDirectory.Documents;

class GeoLogFiles {
    saveCoords = async (lat, lon) => {
        await Toast.show({text: lat + ', ' + lon, duration: 'short'});

        const result = await Filesystem.appendFile({
            path: filePath,
            data: new Date().toString() + ' ' + lat + ',' + lon + '\n\r',
            directory: directory,
            encoding: FilesystemEncoding.UTF8
        });
    }

    clearFile = async () => {
        await Filesystem.deleteFile({
            path: filePath,
            data: '',
            directory: directory,
            encoding: FilesystemEncoding.UTF8
        });
        await Toast.show({text: 'File deleted', duration: 'short'});
    }

    async get() {
        try {
            let fileExist = await this.fileExist();
            if (fileExist) {
                let content = await Filesystem.readFile({
                    path: filePath,
                    directory: directory,
                    encoding: FilesystemEncoding.UTF8
                });

                return content.data;
            } else {
                await Toast.show({text: 'No file found', duration: 'short'});
            }
        } catch (e) {
            await Toast.show({text: 'Unable to get file ' + e, duration: 'short'});

        }
    }

    async fileExist() {
        try {
            let ret = await Filesystem.readdir({
                path: '',
                directory: directory
            });
            for (let i = 0; i < ret.files.length; i++) {
                if (ret.files[i] === filePath) {
                    return true;
                }
            }
        } catch (e) {
            await Toast.show({text: 'Can not get files ' + e, duration: 'short'});

            await Filesystem.mkdir({
                path: "", // корень это пустая строка, а не слеш
                directory: directory,
                recursive: true
            });

            return false;
        }

        return false;
    }
}

export const GeoLogService = new GeoLogFiles();
