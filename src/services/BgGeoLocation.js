import {isPlatform} from "@ionic/react";
import {BackgroundGeolocation, BackgroundGeolocationEvents} from '@ionic-native/background-geolocation/ngx';
import GeoLogService from "./GeoLogStorage";
import {Toast} from "@capacitor/core";

const config = {
    // locationProvider: BackgroundGeolocation.DISTANCE_FILTER_PROVIDER,
    desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
    stationaryRadius: 20,
    distanceFilter: 20,
    notificationTitle: 'Я слежу',
    notificationText: 'За тобой',
    debug: false,
    // stopOnTerminate: false,
    maxLocations: 1000,
    syncThreshold: 100,
    interval: 10000,
    fastestInterval: 5000,
    activitiesInterval: 10000,
    url: 'https://av-promo.ru/test/log.php',
    syncUrl: 'https://av-promo.ru/test/log.php',
    postTemplate: {
        latitude: '@latitude',
        longitude: '@longitude',
        time: '@time'
    },
    httpHeaders: {
        'X-TEST': 'trololo'
    },
    startForeground: true
};

class BgGeoLocation {
    constructor() {
        this.bgGeoLocation = new BackgroundGeolocation();

        if (isPlatform('cordova')) {
            this.bgGeoLocation.configure(config)
                .then(() => {
                    this.bgGeoLocation.on(BackgroundGeolocationEvents.location).subscribe(async (location) => {
                        await GeoLogService.saveCoords(location.latitude, location.longitude);

                        await this.bgGeoLocation.finish();
                    });
                })
                .catch((e) => {
                    console.log('Background geolocation error')
                })
        } else {
            console.log('Not cordova')
        }
    }

    start = async () => {
        if (isPlatform('cordova')) {
            await Toast.show({text: 'Background geolocation start', duration: 'short'});

            await this.bgGeoLocation.start();
        }
    }

    stop = async () => {
        if (isPlatform('cordova')) {
            await Toast.show({text: 'Background geolocation stop', duration: 'short'});

            await this.bgGeoLocation.stop();
            await this.bgGeoLocation.removeAllListeners();
        }
    }

    getUnsentLocations = async () => {
        return await this.bgGeoLocation.getLocations();
    }

    deleteUnsentLocations = async () => {
        await this.bgGeoLocation.deleteAllLocations();
    }
}

export default BgGeoLocation;
