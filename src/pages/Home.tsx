import {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
    IonGrid,
    IonRow,
    IonCol
} from '@ionic/react';
import React, {Component} from 'react';
import GeolocationButton from "../components/GeolocationButton";
import GeolocationLog from "../components/GeolocationLog";
import BackgroundGeolocationControls from "../components/BackgroundGeolocationControls";
import Shot from "../components/Shot";

export class Home extends Component {
    render() {
        return (
            <IonPage>
                <IonHeader>
                    <IonToolbar>
                        <IonTitle>Proof of concept app</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonContent className="ion-padding">
                    <IonGrid>
                        <IonRow>
                            <IonCol size="12" push-md="3" style={{'textAlign': 'center'}}>
                                <GeolocationLog/>
                            </IonCol>
                        </IonRow>
                        <IonRow>
                            <IonCol size="6" push-md="3" style={{'textAlign': 'center'}}>
                                <GeolocationButton/>
                            </IonCol>
                        </IonRow>
                        <IonRow>
                            <IonCol size="6" push-md="3" style={{'textAlign': 'center'}}>
                                <BackgroundGeolocationControls/>
                            </IonCol>
                        </IonRow>
                        <IonRow>
                            <IonCol size="6" push-md="3">
                                <Shot/>
                            </IonCol>
                        </IonRow>
                    </IonGrid>
                </IonContent>
            </IonPage>
        );
    };
}

export default Home;
