Сборка
npm run build
ionic build
npx cap copy android

Перед запуском на симуляторе синхронизируем изменения
npm run build
npx cap copy android
npx cap open android

После установки плагина
npx cap update
npx cap sync android

Запуск веб-версии с горячей заменой
ionic serve

Разработка
Запуск сервера
npx cap serve

// Build web assets
ionic build
// Add android platform
npx cap add android
// Copy all changes to Android platform
npx cap sync
// Open the project in Android studio
npx cap open android

Ошибка импортов при сборке андроида
npx jetifier
